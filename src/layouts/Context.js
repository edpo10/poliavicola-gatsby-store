import React from "react"

const defaultContextValue = {
  data: {
    products: [
      {
        name: "Cartón de huevos",
        price: "2500",
        totalProductsSelected: 1,
        productTotalCart: 0,
      },
      {
        name: "Kilo de huevos",
        price: "1500",
        totalProductsSelected: 1,
        productTotalCart: 0,
      },
      {
        name: "Kilo de huevo empacado",
        price: "2000",
        totalProductsSelected: 1,
        productTotalCart: 0,
      },
      {
        name: "Cartón de huevo empacado",
        price: "3000",
        totalProductsSelected: 1,
        productTotalCart: 0,
      },
    ],
    totalCart: 0,
  },
  set: () => {},
}

const Context = React.createContext(defaultContextValue)

class ContextProviderComponent extends React.Component {
  constructor() {
    super()

    this.setData = this.setData.bind(this)
    this.state = {
      ...defaultContextValue,
      set: this.setData,
    }
  }

  setData(newData) {
    this.setState(state => ({
      data: {
        ...state.data,
        ...newData,
      },
    }))
  }

  render() {
    return (
      <Context.Provider value={this.state}>
        {this.props.children}
      </Context.Provider>
    )
  }
}

export { Context as default, ContextProviderComponent }
