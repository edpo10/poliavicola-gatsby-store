import React from "react"

import styles from "./productsLayout.module.scss"

const productsLayout = ({ children }) => {
  return <section className={styles.productsLayout}>{children}</section>
}

export default productsLayout
