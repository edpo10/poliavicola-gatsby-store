import React, { useState } from "react"
import { Link } from "gatsby"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

import ProductSelect from "./productSelect"
import Button from "./button"
import styles from "./product.module.scss"

const Product = ({
  id,
  name,
  price,
  handleAddToCart,
  storeState,
  handleOptionChange,
  quantity,
}) => {
  const data = useStaticQuery(graphql`
    query {
      placeholderImage: file(relativePath: { eq: "brown-eggs.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 300) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  return (
    <div className={styles.product}>
      <Img fluid={data.placeholderImage.childImageSharp.fluid} />
      <p className={styles.name}>{name}</p>
      <span className={styles.price}>₡{price}</span>
      <div className={styles.cartBtnContainer}>
        <div className={styles.quantityWrapper}>
          <ProductSelect
            id={id}
            value={quantity}
            handleOptionChange={handleOptionChange}
          />
        </div>
        <div>
          <Button
            id={id}
            text="Añadir al carrito"
            handleClick={handleAddToCart}
            quantity={quantity}
          />
        </div>
        <div>
          <Link className={styles.link} to="/carrito/">
            Ir al carrito
        </Link>
        </div>
      </div>
    </div>
  )
}

export default Product
