import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"

import CartIcon from "./cartIcon"
import styles from "./header.module.scss"

const Header = ({ siteTitle, totalCart }) => (
  <header
    className={styles.header}
  >
    <div className={styles.headerContainer}>
      <h1 style={{ margin: 0 }} className={styles.heading}>
        <Link
          to="/"
          style={{
            color: `white`,
            textDecoration: `none`,
          }}
        >
          {siteTitle}
        </Link>
      </h1>
      <CartIcon className={styles.cartIcon} />
    </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
