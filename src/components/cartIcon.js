import React, { useState, useContext } from "react"
import Context from "./../layouts/Context"
import { Link } from "gatsby"

import styles from "./cartIcon.module.scss"
import carrito from "../images/shopping-cart.svg"

const CartIcon = () => {
  const storeData = useContext(Context)
  const totalCart = storeData.data.totalCart
  return (
    <div className={styles.cartIcon}>
      <span>{totalCart}</span>
      <Link to="/carrito/">
        <img
          src={carrito}
          alt="carrito"
        />
      </Link>
    </div>
  )
}

export default CartIcon
