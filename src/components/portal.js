import React from "react"
import ReactDOM from "react-dom"

import styles from "./modal.module.scss"

const portalRoot =
  typeof document !== `undefined` ? document.getElementById("portal") : null

class Portal extends React.Component {
  constructor(props) {
    super(props)
    this.el =
      typeof document !== `undefined` ? document.createElement("div") : null
  }

  componentDidMount() {
    // El elemento del portal se inserta en el árbol DOM después de
    // que se montan los hijos del Modal, lo que significa que los hijos
    // se montarán en un nodo DOM separado. Si un componente hijo
    // requiere estar conectado inmediatamente cuando se monta al árbol del DOM
    // por ejemplo, para medir un nodo DOM, o usar 'autoFocus' en un descendiente,
    // agrega el estado a Modal y renderiza solo a los hijos
    // cuando se inserta Modal en el árbol DOM.
    this.el.className = styles.modalContainer
    portalRoot.appendChild(this.el)
  }

  componentWillUnmount() {
    portalRoot.removeChild(this.el)
  }

  render() {
    const { children } = this.props

    // Check that this.el is not null before using ReactDOM.createPortal
    if (this.el) {
      return ReactDOM.createPortal(children, this.el)
    } else {
      return null
    }
  }
}

export default Portal
