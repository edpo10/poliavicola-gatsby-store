import React, { useState, useContext } from "react"

const ProductSelect = ({ id, value, handleOptionChange }) => {
  const buildOptions = () => {
    const options = []
    for (var i = 1; i <= 100; i++) {
      options.push(
        <option key={i} value={i}>
          {i}
        </option>
      )
    }
    return options
  }
  return (
    <>
      <select id={id} value={value} onChange={handleOptionChange}>
        {buildOptions()}
      </select>
    </>
  )
}

export default ProductSelect
