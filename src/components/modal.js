import React, { useState } from "react"
import Portal from "./portal"
import styles from "./modal.module.scss"

const Modal = ({ children, handleToggleModal }) => (
  <Portal>
    <section className={styles.modal}>
      <div className={styles.modalContainer}>
        <a href="#" className={styles.modalExit} onClick={handleToggleModal}>
          x
        </a>
        {children}
      </div>
    </section>
  </Portal>
)

export default Modal
