import React from "react"
import { Link } from "gatsby"
import styles from "./button.module.scss"

const Button = ({ id, text, quantity, handleClick }) => (
  <button
    className={styles.button}
    onClick={handleClick}
    value={quantity}
    id={id}
  >
    {text}
  </button>
)

export default Button
