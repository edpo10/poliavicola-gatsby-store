import React from "react"
import styles from "./footer.module.scss"

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <div className={styles.footerContent}>
        Poliavícola S.A. Todos los derechos reservados.
      </div>
    </footer>
  )
}

export default Footer
