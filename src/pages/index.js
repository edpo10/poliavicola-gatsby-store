import React, { useState, useContext } from "react"
import Context from "./../layouts/Context"
import { Link } from "gatsby"

import Layout from "../layouts"
import ProductsLayout from "../components/productsLayout"
import Product from "../components/product"
import SEO from "../components/seo"
import productsLayout from "../components/productsLayout"

const IndexPage = () => {
  const storeData = useContext(Context)
  console.log("my context", storeData)

  const handleAddToCart = event => {
    const productId = event.target.id
    const productCartQuantity = parseInt(event.target.value)
    console.log('productCartQuantity ' + productCartQuantity);
    storeData.data.products[productId].productTotalCart += parseInt(
      productCartQuantity
    )
    console.log('storeData.data.products[productId].productTotalCart ' + storeData.data.products[productId].productTotalCart);
    storeData.data.totalCart += productCartQuantity
    storeData.set({ ...storeData })
  }

  const handleOptionChange = event => {
    console.log(event.target.id)
    console.log(event.target.value)
    const productId = event.target.id
    const totalProductsSelected = parseInt(event.target.value)

    storeData.data.products[productId].totalProductsSelected = parseInt(
      totalProductsSelected
    )
    storeData.set({ ...storeData })
  }

  return (
    <Context.Consumer>
      {({ data }) => (
        <>
          <ProductsLayout>
            <SEO title="Home" />
            {data.products.map((product, index) => (
              <Product
                id={index}
                quantity={product.totalProductsSelected}
                price={product.price}
                name={product.name}
                handleOptionChange={handleOptionChange}
                handleAddToCart={handleAddToCart}
              />
            ))}
          </ProductsLayout>
        </>
      )}
    </Context.Consumer>
  )
}
export default IndexPage
