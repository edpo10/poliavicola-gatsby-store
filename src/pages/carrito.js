import React, { useState, useContext } from "react"
import Context from "./../layouts/Context"

import Modal from "../components/modal"
import ProductSelect from "../components/productSelect"

import styles from "./carrito.module.scss"

const Carrito = () => {
  let totalPrice = 0
  const context = useContext(Context)
  const storeData = context.data
  const [showModal, setShowModal] = useState(false)

  const handleOptionChange = event => {
    const productId = event.target.id
    const productCartQuantity = parseInt(event.target.value)
    storeData.totalCart =
      storeData.totalCart - storeData.products[productId].productTotalCart
    storeData.products[productId].productTotalCart = 0
    storeData.products[productId].productTotalCart += parseInt(
      productCartQuantity
    )
    storeData.totalCart += productCartQuantity
    storeData.set({ ...storeData })
  }
  const handleRemoveProduct = event => {
    event.preventDefault()
    const productId = event.target.id
    console.log(event.target)
    storeData.totalCart =
      storeData.totalCart - storeData.products[productId].productTotalCart
    storeData.products[productId].productTotalCart = 0
    storeData.set({ ...storeData })
  }
  const handleToggleModal = event => {
    event.preventDefault()
    setShowModal(!showModal)
  }
  const handleOnSubmit = event => {
    event.preventDefault()
  }
  return storeData.totalCart !== 0 ? (
    <>
      <section className={styles.carrito}>
        <table>
          <tr>
            <th>Producto</th>
            <th>Precio</th>
            <th>Cantidad</th>
            <th>Remover</th>
            <th>Total</th>
          </tr>
          {storeData.products
            .filter((product, index) => {
              product.id = index
              return product.productTotalCart
            })
            .map(product => {
              const productTotalPrice =
                parseInt(product.price) * parseInt(product.productTotalCart)
              totalPrice =
                totalPrice +
                parseInt(product.price) * parseInt(product.productTotalCart)
              return (
                <>
                  <tr>
                    <td>{product.name}</td>
                    <td>₡{product.price}</td>
                    <td>
                      <ProductSelect
                        id={product.id}
                        value={product.productTotalCart}
                        handleOptionChange={handleOptionChange}
                      />
                      {/*<select>
                      <option>{product.productTotalCart}</option>
                    </select>*/}
                    </td>
                    <td>
                      <a id={product.id} href="#" onClick={handleRemoveProduct}>
                        Remover
                      </a>
                    </td>
                    <td>₡{productTotalPrice}</td>
                  </tr>
                </>
              )
            })}
        </table>
        <p>Total: ₡{totalPrice}</p>
        <a href="#" onClick={handleToggleModal}>
          Pagar
        </a>
        {showModal && (
          <Modal handleToggleModal={handleToggleModal}>
            <div className={styles.modalCheckout}>
              <h2>Gracias por su compra!</h2>
              <p>
                En este momento manejemos el pago por transferencia electrónica,
                para realizar el envío necesitamos los siguientes datos:
              </p>
              <div className={styles.modalCheckoutContainer}>
                <div className={styles.modalCheckoutForm}>
                  <h3>Datos de envío</h3>
                  <form onSubmit={handleOnSubmit}>
                    <div>
                      <label htmlFor="name">Nombre </label>
                      <input type="text" value="" name="name" />
                    </div>
                    <div>
                      <label htmlFor="email">Correo Electrónico </label>
                      <input type="text" value="" name="email" />
                    </div>
                    <div>
                      <label htmlFor="address">Dirección </label>
                      <textarea value="" name="address" />
                    </div>
                  </form>
                </div>
                <div className={styles.modalCheckoutPayment}>
                  <h3>Números de cuenta</h3>
                  <h4>BAC San José</h4>
                  <p>
                    <strong>Cuenta cliente:</strong> 123456677
                  </p>
                  <p>
                    <strong>IBAN:</strong> 122334456
                  </p>
                </div>
              </div>
            </div>
          </Modal>
        )}
      </section>
    </>
  ) : (
      <section className={styles.carrito}>
        <h1>Su carrito está vacío</h1>
      </section>
    )
}

export default Carrito
